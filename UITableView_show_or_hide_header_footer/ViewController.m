#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, readonly, strong) UITableView *tableView;
@property (nonatomic, readonly, strong) UIView *header;
@property (nonatomic, readonly, strong) UIView *footer;

@end

@implementation ViewController

CGFloat prevContentOffsetY = -1;

static CGFloat statusBarHeight = 20;
static CGFloat headerHeight = 44;
static CGFloat footerHeight = 44;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView = [[UITableView alloc] init];
    _tableView.backgroundColor = [UIColor redColor];
    _tableView.frame = CGRectMake(0, statusBarHeight, self.view.frame.size.width, self.view.frame.size.height - statusBarHeight);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    _header = [[UIView alloc] init];
    _header.backgroundColor = [UIColor greenColor];
    _header.frame = CGRectMake(0, statusBarHeight, self.view.frame.size.width, headerHeight);
    [self.view addSubview:_header];
    
    _footer = [[UIView alloc] init];
    _footer.backgroundColor = [UIColor blueColor];
    _footer.frame = CGRectMake(0, self.view.frame.size.height - footerHeight, self.view.frame.size.width, footerHeight);
    [self.view addSubview:_footer];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark
#pragma mark UITableView delegate datasource
#pragma mark
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ 番目のデータ", @(indexPath.row + 1)];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint currentOffset = scrollView.contentOffset;
    CGFloat currentOffsetY = currentOffset.y;
    
//    NSLog(@"currentOffsetY : %@", @(currentOffsetY));
//    NSLog(@"scrollView.contentSize : %@", NSStringFromCGSize(scrollView.contentSize));
//    NSLog(@"scrollView.contentSize.height - scrollView.frame.size.height : %@", @(scrollView.contentSize.height - scrollView.frame.size.height)); // スクロールできる量を確認
    
    // 上部のバウンス
    if (currentOffsetY < 0) {
        return;
    }
    
    // 下部のバウンス
    //if (currentOffsetY > 1553) {
    if (currentOffsetY > scrollView.contentSize.height - scrollView.frame.size.height) {
        return;
    }
    
    if (prevContentOffsetY < currentOffsetY) {
//        NSLog(@"下にスクロール");
        // ヘッダー
        {
            CGFloat diffY = currentOffsetY - prevContentOffsetY;
            CGRect frame = self.header.frame;
            CGPoint origin = frame.origin;
            CGSize size = frame.size;
            CGFloat originX = origin.x;
            CGFloat originY = origin.y;
            if (originY > -headerHeight) {
                originY -= diffY;
                frame = CGRectMake(originX, originY, size.width, size.height);
                self.header.frame = frame;
            }
        }
        // フッター
        {
            CGFloat diffY = currentOffsetY - prevContentOffsetY;
            CGRect frame = self.footer.frame;
            CGPoint origin = frame.origin;
            CGSize size = frame.size;
            CGFloat originX = origin.x;
            CGFloat originY = origin.y;
            if (originY <= self.view.frame.size.height) {
                originY += diffY;
                frame = CGRectMake(originX, originY, size.width, size.height);
                self.footer.frame = frame;
            }
        }
    } else {
//        NSLog(@"上にスクロール");
        // ヘッダー
        {
            CGFloat diffY = prevContentOffsetY - currentOffsetY;
            CGRect frame = self.header.frame;
            CGPoint origin = frame.origin;
            CGSize size = frame.size;
            CGFloat originX = origin.x;
            CGFloat originY = origin.y;
            if (originY < 0) {
                originY += diffY;
                if (originY > 0) {
                    originY = 0;
                }
                frame = CGRectMake(originX, originY, size.width, size.height);
                self.header.frame = frame;
            }
        }
        // フッター
        {
            CGFloat diffY = prevContentOffsetY - currentOffsetY;
            CGRect frame = self.footer.frame;
            CGPoint origin = frame.origin;
            CGSize size = frame.size;
            CGFloat originX = origin.x;
            CGFloat originY = origin.y;
            if (originY > self.view.frame.size.height - footerHeight) {
                originY -= diffY;
                if (originY < self.view.frame.size.height - footerHeight) {
                    originY = self.view.frame.size.height - footerHeight;
                }
                frame = CGRectMake(originX, originY, size.width, size.height);
                self.footer.frame = frame;
            }
        }
    }
    
    prevContentOffsetY = currentOffsetY;
}

@end
